var correlationId = context.getVariable("request.queryparam.correlationId");
var routingNumber = context.getVariable("request.queryparam.routingNumber");
var accountNumber = context.getVariable("request.queryparam.accountNumber");
var payload = JSON.parse(context.getVariable("response.content"));
var timestamp = new Date().toISOString().replace('Z', '');

var finalResponse = {};

if (correlationId !== null) {
    finalResponse.correlationId = correlationId;
} else {
    finalResponse.correlationId = "rrt-0006196d652852225-d-ea-19546-23076180-1";
}
finalResponse.routingNumber = routingNumber;
finalResponse.accountNumber = accountNumber;
finalResponse.validationStatus = payload.calculated_response;
finalResponse.timestamp = timestamp;

context.setVariable("finalResponse", JSON.stringify(finalResponse));
