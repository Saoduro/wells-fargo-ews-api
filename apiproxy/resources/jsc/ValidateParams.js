var correlationId = context.getVariable("request.queryparam.correlationId");
var routingNumber = context.getVariable("request.queryparam.routingNumber");
var accountNumber = context.getVariable("request.queryparam.accountNumber");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);
context.setVariable("correlationId", correlationId);

context.setVariable("invalidParam", "false");

if (correlationId !== null && correlationId.trim() === "") {
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Please provide a valid correlationId");
    context.setVariable("queryParam", "correlationId");
    context.setVariable("correlationId", "rrt-0006196d652852225-d-ea-19546-23076180-1");
    
} else if (routingNumber === null || routingNumber.trim() === "") {
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Please provide a valid routingNumber");
    context.setVariable("queryParam", "routingNumber");
    
} else if (accountNumber === null || accountNumber.trim() === "") {
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Please provide a valid accountNumber");
    context.setVariable("queryParam", "accountNumber");
    
}
